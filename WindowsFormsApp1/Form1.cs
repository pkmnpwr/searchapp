﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;


namespace searchApp
{
    public partial class Form1 : Form
    {
        string rootPath;
        string _searchPattern = "*";
        int doneFilesInDir = 0;
        int foundFiles = 0;
        int doneFiles = 0;
        int doneDirs = 0;
        bool workDone = false;
        bool searchText = false;
        Stopwatch watch = new Stopwatch();
        bool onLoad = true;
        Queue<TreeNode> queue ;
        public Form1()
        {
            InitializeComponent();
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            startAsyncButton.Enabled = true;
            cancelAsyncButton.Enabled = false;
            resetButton.Enabled = false;
            rootDir.Text = Properties.Settings.Default.root;
            searchBox.Text = Properties.Settings.Default.pattern;
            searchBox2.Text = Properties.Settings.Default.text;
            queue = new Queue<TreeNode>();
            onLoad = false;
        }
        /// <summary>
        /// Кнопка старта/продолжения
        /// </summary>
        private void startAsyncButton_Click(object sender, EventArgs e)
        {
            startAsyncButton.Enabled = false;
            cancelAsyncButton.Enabled = true;
            resetButton.Enabled = true;
            if (workDone)
            {
                reset();
            }
            watch.Start();
            if (searchBox2.Text != "")
                searchText = true;
            backgroundWorker1.RunWorkerAsync();
        }
        /// <summary>
        /// Кнопка паузы
        /// </summary>
        private void cancelAsyncButton_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                cancelAsyncButton.Enabled = false;
                backgroundWorker1.CancelAsync();
                while (backgroundWorker1.IsBusy)
                    Application.DoEvents();
                startAsyncButton.Enabled = true;                
            }
        }
        /// <summary>
        /// Поиск
        /// </summary>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (!Directory.Exists(rootPath))
            {
                if (rootEx.IsHandleCreated)
                    BeginInvoke(new Action(() => rootEx.Text = "Неверный стартовый путь"));
                return;
            }
            else
            {
                if (rootEx.IsHandleCreated)
                    BeginInvoke(new Action(() => rootEx.Text = ""));
            }
            bool checkFile = false;
            //


            if (queue.Count == 0)
            {
                var rootDirectory = new DirectoryInfo(rootPath);
                var node = new TreeNode(rootDirectory.Name) { Tag = rootDirectory };
                queue.Enqueue(node);
            }
            
            while (queue.Count > 0)
            {
                var currentNode = queue.Dequeue();
                var directoryInfo = (DirectoryInfo)currentNode.Tag;
                bool check = false;
                try
                {
                    directoryInfo.GetDirectories();
                    check = true;
                }
                catch { }
                if (check)
                {
                    foreach (string dInfo in Directory.EnumerateDirectories(directoryInfo.FullName, "*"))
                    {
                        var childDirectoryNode = new TreeNode(dInfo) { Tag = new DirectoryInfo(dInfo) };
                        currentNode.Nodes.Add(childDirectoryNode);
                        queue.Enqueue(childDirectoryNode);
                    }
                    foreach (string d in Directory.EnumerateFiles(directoryInfo.FullName, _searchPattern).Skip(doneFilesInDir))
                    {
                        if (worker.CancellationPending == true)
                        {
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            ++doneFiles;

                            BeginInvoke(new Action(() => timeLabel.Text = watch.Elapsed.ToString()));
                            BeginInvoke(new Action(() => resultLabel.Text = Path.GetFileName(d)));
                            BeginInvoke(new Action(() => doneFile.Text = doneFiles.ToString()));
                            Thread.Sleep(2);
                            if (searchText)
                            {
                                try
                                {
                                    File.ReadAllText(d);
                                    checkFile = true;
                                }
                                catch { checkFile = false; }

                                if (checkFile && File.ReadAllText(d).ToLower().Contains(searchBox2.Text.ToLower()))
                                {
                                    worker.ReportProgress(0, d);                                    
                                    ++doneFilesInDir;
                                    ++foundFiles;
                                    BeginInvoke(new Action(() => foundFilesLabel.Text = foundFiles.ToString()));
                                }
                            }
                            else
                            {
                                worker.ReportProgress(0, d);
                                ++doneFilesInDir;
                                ++foundFiles;
                                BeginInvoke(new Action(() => foundFilesLabel.Text = foundFiles.ToString()));
                                BeginInvoke(new Action(() => timeLabel.Text = watch.Elapsed.ToString()));
                            }
                        }
                    }
                    doneFilesInDir = 0;
                }
                ++doneDirs;
            }            
        }
        /// <summary>
        /// Отрисовка UI при изменении прогресса worker'a
        /// </summary>
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        { 
            refreshTreeView(e.UserState.ToString());
        }
        /// <summary>
        /// Отрисовка UI при остановке/завершении работы worker'a
        /// </summary>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            watch.Stop();
            TimeSpan ts = watch.Elapsed;
            timeLabel.Text = ts.ToString();
            startAsyncButton.Enabled = true;
            cancelAsyncButton.Enabled = false;
            if (e.Cancelled == true)
            {
                workDone = false;
                resultLabel.Text = "Paused!";
            }
            else if (e.Error != null)
            {
                resultLabel.Text = "Error: " + e.Error.Message;
            }
            else
            {
                workDone = true;
                resultLabel.Text = "Done!";
            }
        }
        /// <summary>
        /// Запуск worker'a при изменении поискового запроса1
        /// </summary>
        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            if (!onLoad)
            {
                reset();
                _searchPattern = "*" + searchBox.Text + "*";
                queue = new Queue<TreeNode>();
                if (!onLoad && (searchBox.Text != "" || searchBox2.Text != ""))
                {
                    startAsyncButton.Enabled = false;
                    cancelAsyncButton.Enabled = true;
                    resetButton.Enabled = true;
                    watch.Start();
                    backgroundWorker1.RunWorkerAsync();
                }
            }
        }
        /// <summary>
        /// Запуск worker'a при изменении поискового запроса2
        /// </summary>
        private void searchBox2_TextChanged(object sender, EventArgs e)
        {
            if (!onLoad)
            {
                reset();
                if (searchBox2.Text != "")
                    searchText = true;
                else
                    searchText = false;
                queue = new Queue<TreeNode>();
                if (!onLoad && (searchBox.Text != "" || searchBox2.Text != ""))
                {
                    startAsyncButton.Enabled = false;
                    cancelAsyncButton.Enabled = true;
                    resetButton.Enabled = true;
                    watch.Start();
                    backgroundWorker1.RunWorkerAsync();
                }
            }
        }
        /// <summary>
        /// Кнопка ресета UI
        /// </summary>
        private void resetButton_Click(object sender, EventArgs e)
        {
            reset();
            queue = new Queue<TreeNode>();
            resetButton.Enabled = false;
        }
        /// <summary>
        /// Остановка worker'a при изменении пути
        /// </summary>
        private void rootDir_TextChanged(object sender, EventArgs e)
        {
            reset();
            queue = new Queue<TreeNode>();
            rootPath = rootDir.Text;
            cancelAsyncButton.Enabled = false;

        }
        /// <summary>
        /// Функция ресета UI
        /// </summary>
        private void reset()
        {
            watch.Stop();
            watch.Reset();
            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                backgroundWorker1.CancelAsync();
            }
            while (backgroundWorker1.IsBusy)
                Application.DoEvents();
            treeView1.Nodes.Clear();
            //root.Nodes.Clear();
            //treeView1.Nodes.Add(root);
            doneFilesInDir = 0;
            doneDirs = 0;
            foundFiles = 0;
            doneFiles = 0;
            timeLabel.Text = "0";
            foundFilesLabel.Text = "0";
            doneFile.Text = "0";
            resultLabel.Text = "";
            queue = null;
        }
        /// <summary>
        /// Сохранение введеных критериев
        /// </summary>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default["root"] = rootDir.Text;
            Properties.Settings.Default["pattern"] = searchBox.Text;
            Properties.Settings.Default["text"] = searchBox2.Text;
            Properties.Settings.Default.Save();
        }
        /// <summary>
        /// Выбор стартового каталога для поиска
        /// </summary>
        private void folderDialogButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = rootDir.Text;
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    rootPath = fbd.SelectedPath;                  
                    treeView1.Nodes.Clear();                    
                    rootDir.Text = rootPath;
                }
            }
        }

        /// <summary>
        /// Отрисовка treeView
        /// </summary>
        private void refreshTreeView(string path)
        {
            //TreeNode node = new TreeNode();
            //string[] pathes = path.Split('\\');
            //treeView1.Nodes.(pathes[0], true);
            string[] split = path.Split('\\'); //Split path into dir names and file name
            string splitname = "";
            split = split.Skip(Array.IndexOf(split, Path.GetFileName(rootPath))).ToArray();
            TreeNode savedresult = null; bool findlastresult = false;
            foreach (string splitpart in split) //Code for realtime refreshing treeview and don't adding empty folders
            {
                splitname += splitpart + "\\";
                TreeNode[] result;
                if (findlastresult)
                    result = savedresult.Nodes.Find(splitname, true);
                else
                    result = treeView1.Nodes.Find(splitname, true);
                if (result.Length == 0)
                {
                    if (findlastresult)
                         savedresult.Nodes.Add(splitname, splitpart);
                    else
                        treeView1.Nodes.Add(splitname, splitpart);
                    result = treeView1.Nodes.Find(splitname, true);
                }
                savedresult = result[0];
                findlastresult = true;
            }
            treeView1.Nodes[0].Expand();
        }
    }
}